#include "ADC.h"

void ADC1_2_IRQHandler(void) {
	ADC_FLAG = 1;
	ADC_VAL = ADC1_DR;
	NVIC_CLEAR_PEND = 1 << 18;//Clear interrupt pending flag
}

void ADCInit(void) {
	//Our only interesting channel is channel 10
	ADC1_SQR3 |= 10;	//Channel 10
	ADC1_SQR1 |= 1;		//Only 1 conversion in sequence
	ADC1_SMPR1 |= 5;	//55.5 cycles per conversion on channel 10
	ADC1_CR1 |= 1 << 8 |//Scan mode
							1 << 5 |//EOC interrupt enable
							1 << 20;//External interrupt enable
	ADC1_CR2 	|= 1 |		//Enable ADC
							1 << 22;//SWSTART bit
	
	//Calibration
	ADC1_CR2 |= 1 << 3;//Reset calibration
	while (ADC1_CR2 & (1 << 3));//Wait for bit to go low
	ADC1_CR2 |= 1 << 2;//Calibration
	while (ADC1_CR2 & (1 << 2));//Wait for calibration to finish
}

void EnableADCInt (void) {
	//Enable interrupt in NVIC
	NVIC_SET_EN = 1 << 18;
}

void DisableADCInt(void) {
	//Disable interrupt in NVIC
	NVIC_CLEAR_EN = 1 << 18;
}

//Start conversion and leave
void StartADC(void) {
	ADC1_CR2 |= 1;					//Start conversion
}

//Ask for conversion, wait for ADC to be ready, then return value
uint16_t GetADC(void) {
	while(!(ADC1_SR & 2));	//Wait for EOC
	return ADC1_DR;					//Return converted value
}
