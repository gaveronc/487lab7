#include "timer.h"

void InitPWMTimer(void) {
	RCC_APB1ENR |= 1 << 2;			//Enable clock to TIM4
	TIM4_CCMR1 |= 	6 << 12|//Channel 2 PWM Mode
					1 << 10;//Channel 2 Fast compare enable
	
	TIM4_PSC = 71;				//Divide clock down to 1MHz
	TIM4_ARR = 20000;			//20,000uS (max period)
	TIM4_DIER |= 1;				//Enable update interrupt
	TIM4_CCR2 = 600;			//0 Degrees default
	TIM4_CR1 |= 1;  			//Enable timer
}

void PWMTimerStart(void) {
	TIM4_CCER |= 1 << 4;	//Channel 2 output enable
}

void PWMTimerStop(void) {
	TIM4_CCER &= ~(1 << 4);	//Disable channel 2 output
	GPIOB_ODR &= ~(1 << 6);	//Set output to 0
}

void PWMAngleSet(uint16_t percent) {
	//Put the percentage into the range of 600-2,400uS
	TIM4_CCR2 = (uint16_t)(600 + ((((float)percent)/10000)*1800));
}
