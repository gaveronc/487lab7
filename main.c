/*
    FreeRTOS V7.2.0 - Copyright (C) 2012 Real Time Engineers Ltd.


    ***************************************************************************
     *                                                                       *
     *    FreeRTOS tutorial books are available in pdf and paperback.        *
     *    Complete, revised, and edited pdf reference manuals are also       *
     *    available.                                                         *
     *                                                                       *
     *    Purchasing FreeRTOS documentation will not only help you, by       *
     *    ensuring you get running as quickly as possible and with an        *
     *    in-depth knowledge of how to use FreeRTOS, it will also help       *
     *    the FreeRTOS project to continue with its mission of providing     *
     *    professional grade, cross platform, de facto standard solutions    *
     *    for microcontrollers - completely free of charge!                  *
     *                                                                       *
     *    >>> See http://www.FreeRTOS.org/Documentation for details. <<<     *
     *                                                                       *
     *    Thank you for using FreeRTOS, and thank you for your support!      *
     *                                                                       *
    ***************************************************************************


    This file is part of the FreeRTOS distribution.

    FreeRTOS is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License (version 2) as published by the
    Free Software Foundation AND MODIFIED BY the FreeRTOS exception.
    >>>NOTE<<< The modification to the GPL is included to allow you to
    distribute a combined work that includes FreeRTOS without being obliged to
    provide the source code for proprietary components outside of the FreeRTOS
    kernel.  FreeRTOS is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details. You should have received a copy of the GNU General Public
    License and the FreeRTOS license exception along with FreeRTOS; if not it
    can be viewed here: http://www.freertos.org/a00114.html and also obtained
    by writing to Richard Barry, contact details for whom are available on the
    FreeRTOS WEB site.

    1 tab == 4 spaces!
    
    ***************************************************************************
     *                                                                       *
     *    Having a problem?  Start by reading the FAQ "My application does   *
     *    not run, what could be wrong?                                      *
     *                                                                       *
     *    http://www.FreeRTOS.org/FAQHelp.html                               *
     *                                                                       *
    ***************************************************************************

    
    http://www.FreeRTOS.org - Documentation, training, latest information, 
    license and contact details.
    
    http://www.FreeRTOS.org/plus - A selection of FreeRTOS ecosystem products,
    including FreeRTOS+Trace - an indispensable productivity tool.

    Real Time Engineers ltd license FreeRTOS to High Integrity Systems, who sell 
    the code with commercial support, indemnification, and middleware, under 
    the OpenRTOS brand: http://www.OpenRTOS.com.  High Integrity Systems also
    provide a safety engineered and independently SIL3 certified version under 
    the SafeRTOS brand: http://www.SafeRTOS.com.
*/

/*
 * This is a very simple demo that demonstrates task and queue usages only in
 * a simple and minimal FreeRTOS configuration.  Details of other FreeRTOS 
 * features (API functions, tracing features, diagnostic hook functions, memory
 * management, etc.) can be found on the FreeRTOS web site 
 * (http://www.FreeRTOS.org) and in the FreeRTOS book.
 *
 * Details of this demo (what it does, how it should behave, etc.) can be found
 * in the accompanying PDF application note.
 *
*/

/* Kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"

/* Standard include. */
#include <stdio.h>

/* Priorities at which the tasks are created. */
#define mainQUEUE_RECEIVE_TASK_PRIORITY     ( tskIDLE_PRIORITY + 2 )
#define mainQUEUE_SEND_TASK_PRIORITY        ( tskIDLE_PRIORITY + 1 )
#define ControlTask_PRIORITY                  	( tskIDLE_PRIORITY + 3 )
#define RXTask_PRIORITY                 		( tskIDLE_PRIORITY + 4 )

/* The rate at which data is sent to the queue, specified in milliseconds. */
#define mainQUEUE_SEND_FREQUENCY_MS         ( 10 / portTICK_RATE_MS )

/* The number of items the queue can hold.  This is 1 as the receive task
will remove items as they are added, meaning the send task should always find
the queue empty. */
#define mainQUEUE_LENGTH                    ( 1 )

/* The ITM port is used to direct the printf() output to the serial window in 
the Keil simulator IDE. */
#define mainITM_Port8(n)    (*((volatile unsigned char *)(0xE0000000+4*n)))
#define mainITM_Port32(n)   (*((volatile unsigned long *)(0xE0000000+4*n)))
#define mainDEMCR           (*((volatile unsigned long *)(0xE000EDFC)))
#define mainTRCENA          0x01000000

/*-----------------------------------------------------------*/

/*
 * The tasks as described in the accompanying PDF application note.
 */
static void prvQueueReceiveTask( void *pvParameters );
static void prvQueueSendTask( void *pvParameters );
static void ControlTask( void *pvParameters );
static void RXTask( void *pvParameters );

/*
 * Redirects the printf() output to the serial window in the Keil simulator
 * IDE.
 */
int fputc( int iChar, FILE *pxNotUsed );

/*-----------------------------------------------------------*/

/* The queue used by both tasks. */
static xQueueHandle xQueue = NULL;

/* One array position is used for each task created by this demo.  The 
variables in this array are set and cleared by the trace macros within
FreeRTOS, and displayed on the logic analyzer window within the Keil IDE -
the result of which being that the logic analyzer shows which task is
running when. */
unsigned long ulTaskNumber[ configEXPECTED_NO_RUNNING_TASKS ];

/*-----------------------------------------------------------*/
//#include "main.h"
#include <stdint.h>
#include "registers.h"
#include "UART.h" //Enable UART functions
#include "commands.h"
#include "timer.h"
#include "ADC.h"
#include "control.h"
#include "SeaBiscuit.h"

void SysInit(void) {
	//Need to setup the the LED's on the board
	RCC_APB2ENR |= 	1 << 3 |	// Enable Port B clock
									1 << 4 | 	// Enable Port C clock
									1 << 9;		// Enable ADC1 clock
	GPIOB_ODR  &= ~0x0000FF00; // Switch off LEDs
	GPIOB_CRL  = 0xB3333333;
	GPIOB_CRH  = 0x33333333; //50MHz  General Purpose output push-pull
	
	//Set up timers
	InitPWMTimer();
	
	//Initialize ADC
	ADCInit();
	
	Serial_Open();//Start serial communications
}

struct command_struct my_config;

int main(void)
{
	//Initialize clocks and GPIO pins
	SysInit();
	
	//Configure SeaBiscuit command line
	my_config.sea_name = "SeaBiscuit";
	my_config.sea_command_length = COMMAND_LENGTH;
	my_config.sea_command_num = COMMAND_LIST_LENGTH + 2;
	my_config.sea_command_history_size = COMMAND_BUFFER_LENGTH;
	my_config.sea_echo_cmd = SendByte;//SendByte sends a single character over UART
	
	my_config.sea_command_list = malloc((COMMAND_LIST_LENGTH + 2) * sizeof(char *));
	my_config.sea_function_calls = malloc((COMMAND_LIST_LENGTH + 2) * sizeof(void *));
	
	//Build command list
	my_config.sea_command_list[0] = "LED ON ";
	my_config.sea_function_calls[0] = LED_ON;
	my_config.sea_command_list[1] = "LED OFF ";
	my_config.sea_function_calls[1] = LED_OFF;
	my_config.sea_command_list[2] = "HELP";
	my_config.sea_function_calls[2] = HELP;
	my_config.sea_command_list[3] = "QUERY LED ";
	my_config.sea_function_calls[3] = QUERY_LED;
	my_config.sea_command_list[4] = "INFO";
	my_config.sea_function_calls[4] = INFO;
	my_config.sea_command_list[5] = "SET ANGLE ";
	my_config.sea_function_calls[5] = SETANGLE;
	my_config.sea_command_list[6] = "VALVE ON";
	my_config.sea_function_calls[6] = VALVEON;
	my_config.sea_command_list[7] = "VALVE OFF";
	my_config.sea_function_calls[7] = VALVEOFF;
	my_config.sea_command_list[8] = "ADC READ";
	my_config.sea_function_calls[8] = ADCREAD;
	my_config.sea_command_list[9] = "CONTROL ON ";
	my_config.sea_function_calls[9] = CONTROL_ON;
	my_config.sea_command_list[10] = "CONTROL OFF";
	my_config.sea_function_calls[10] = CONTROL_OFF;
	my_config.sea_command_list[11] = "HISTORY";
	my_config.sea_function_calls[11] = sea_history_cmd;
	my_config.sea_command_list[12] = "!";
	my_config.sea_function_calls[12] = sea_bang_function;
	
	//Give the configuration to SeaBiscuit
	InitBiscuit(&my_config);

    /* Create the queue. */
    xQueue = xQueueCreate( mainQUEUE_LENGTH, sizeof( unsigned long ) );

    if( xQueue != NULL )
    {
        /* Start the two tasks as described in the accompanying application
        note. */
//         xTaskCreate( prvQueueReceiveTask, ( signed char * ) "Rx",
//                  configMINIMAL_STACK_SIZE, NULL,
//                  mainQUEUE_RECEIVE_TASK_PRIORITY, NULL );
//         xTaskCreate( prvQueueSendTask, ( signed char * ) "TX",
//                  configMINIMAL_STACK_SIZE, NULL,
//                  mainQUEUE_SEND_TASK_PRIORITY, NULL );

        xTaskCreate( ControlTask, (signed char *) "ControlTask",
                 configMINIMAL_STACK_SIZE, NULL,
                 ControlTask_PRIORITY, NULL );
        xTaskCreate( RXTask, (signed char *) "RXTask",
                 configMINIMAL_STACK_SIZE, NULL,
                 RXTask_PRIORITY, NULL );
        /* Start the tasks running. */
        vTaskStartScheduler();
    }

    /* If all is well we will never reach here as the scheduler will now be
    running.  If we do reach here then it is likely that there was insufficient
    heap available for the idle task to be created. */
    for( ;; )
        ;
}
/*-----------------------------------------------------------*/

static void ControlTask( void *pvParameters )
{
    portTickType xNextWakeTime = xTaskGetTickCount();
    
    while (1) {
        if(ADC_FLAG) {
					PControl();
					StartADC();
				}
        vTaskDelayUntil(&xNextWakeTime, 2);
    }

}
static void RXTask( void *pvParameters )
{
    portTickType xNextWakeTime = xTaskGetTickCount();
    
    while (1) {
        if (ByteReady()) {
					ParseByte(GetByte());
				}
        vTaskDelayUntil(&xNextWakeTime, 1);
    }

}


static void prvQueueSendTask( void *pvParameters )
{
portTickType xNextWakeTime;
const unsigned long ulValueToSend = 100UL;

    /* Initialise xNextWakeTime - this only needs to be done once. */
    xNextWakeTime = xTaskGetTickCount();

    for( ;; )
    {
        /* Place this task in the blocked state until it is time to run again.
        The block time is specified in ticks, the constant used converts ticks
        to ms.  While in the Blocked state this task will not consume any CPU
        time. */
        vTaskDelayUntil( &xNextWakeTime, mainQUEUE_SEND_FREQUENCY_MS );

        /* Send to the queue - causing the queue receive task to unblock and
        print out a message.  0 is used as the block time so the sending 
        operation will not block - it shouldn't need to block as the queue 
        should always be empty at this point in the code. */
        xQueueSend( xQueue, &ulValueToSend, 0 );
    }
}
/*-----------------------------------------------------------*/

static void prvQueueReceiveTask( void *pvParameters )
{
unsigned long ulReceivedValue;

    for( ;; )
    {
        /* Wait until something arrives in the queue - this task will block
        indefinitely provided INCLUDE_vTaskSuspend is set to 1 in
        FreeRTOSConfig.h. */
        xQueueReceive( xQueue, &ulReceivedValue, portMAX_DELAY );

        /*  To get here something must have been received from the queue, but
        is it the expected value?  If it is, print out a pass message, if no,
        print out a fail message. */
        if( ulReceivedValue == 100UL )
        {
            printf( "Value 100 received - Tick = 0x%x\r\n", (unsigned int)xTaskGetTickCount() );
        }
        else
        {
            printf( "Received an unexpected value\r\n" );
        }
    }
}
/*-----------------------------------------------------------*/

int fputc( int iChar, FILE *pxNotUsed ) 
{
    /* Just to avoid compiler warnings. */
    ( void ) pxNotUsed;

    if( mainDEMCR & mainTRCENA ) 
    {
        while( mainITM_Port32( 0 ) == 0 );
        mainITM_Port8( 0 ) = iChar;
    }

    return( iChar );
}
