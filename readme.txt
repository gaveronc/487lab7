The code for this project is a slightly modified version of the FreeRTOS demo
code. The only real changes are what the tasks are doing; rather than toggling
LEDs at different frequencies, one task now handles the command line interface,
and the other handles the valve control algorithm.

ADC input is still handled by an interrupt as it was in the previous lab. While
the handout suggested breaking the system down into 4 tasks, I felt such a
decomposition was entirely unnecessary, and would've only complicated the
system.
