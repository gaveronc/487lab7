#include "commands.h"

//Turn on some LEDs
void LED_ON (char * LED) {
	//Turn on an LED
	if (LED[0] < '0' || LED[0] > '8' || LED[1] != 0) {
		SendLine("Argument is not an LED\r\n");
		return;
	}
	if (LED[0] == '0') {//Turn on all LEDs
		GPIOB_ODR |= 0xFF00;
	} else {
		GPIOB_ODR |= 1 << (LED[0] - 41);
	}
}

//Turn off some LEDs
void LED_OFF (char * LED) {
	//Turn off an LED
	if (LED[0] < '0' || LED[0] > '8' || LED[1] != 0) {
		SendLine("Argument is not an LED\r\n");
		return;
	}
	if (LED[0] == '0') {
		GPIOB_ODR &= ~(0xFF00);
	} else {
		GPIOB_ODR &= ~(1 << (LED[0]-41));
	}
}

//Describe commands
void HELP (char * data) {
	if(data[0] != 0) {
		SendLine("Ignoring arguments\r\n");
	}
	SendLine("This is the help menu\r\n");
	SendLine("\"HISTORY\" displays the previous commands indexed in order\r\n");
	SendLine("\"!\" performs the previous command at index 0\r\n");
	SendLine("\"!X\" performs previous command at index X\r\n");
	SendLine("\"LED ON X\" will turn on LED X\r\n");
	SendLine("\"LED OFF X\" will turn off LED X\r\n");
	SendLine("\"LED ON 0\" will turn on all LEDs\r\n");
	SendLine("\"LED OFF 0\" will turn off all LEDs\r\n");
	SendLine("\"QUERY LED X\" will report the status of LED X\r\n");
	SendLine("\"SET ANGLE XYZ\" sets the angle of the valve to the number XYZ\r\n");
	SendLine("\"VALVE ON\" turns valve controls on\r\n");
	SendLine("\"VALVE OFF\" turns valve controls off\r\n");
	SendLine("\"ADC READ\" prints the value of the ADC\r\n");
	SendLine("\"CONTROL ON ABCD\" turns on the automated valve controls and\r\n");
	SendLine("tries to hold the pong ball at AB.CD cm (ABCD is the desired\r\n");
	SendLine("height * 100)\r\n");
	SendLine("\"CONTROL OFF\" turns off the automated valve controls\r\n");
	SendLine("\"INFO\" prints date and time of compilation\r\n");
	SendLine("\"HELP\" prints this help message\r\n");
}

//Return state of LED
void QUERY_LED (char * LED) {
	if (LED[1] != 0 || LED[0] < '1' || LED[0] > '8') {
		SendLine("Argument is not an LED\r\n");
		return;
	}
	if (GPIOB_IDR & (1 << (LED[0]-41))) {
		SendLine("The LED is on\r\n");
	} else {
		SendLine("The LED is off\r\n");
	}
}

//Set angle of valve on equipment
void SETANGLE (char * data) {
	int percent = StringToInt(data);
	
	if(percent <= 10000 && percent != -1)
	{
		PWMAngleSet(percent);
		SendLine("Angle set to ");
		SendInt(percent);
		SendLine("%\r\n");
	} else {
		SendLine("Invalid value entered: ");
		SendLine(data);
		SendLine("\r\n");
	}
}

//Enable valve system
void VALVEON (char * data) {
	if(data[0] != 0)
		SendLine("Ignoring arguments\r\n");
	PWMTimerStart();
	SendLine("Timer started\r\n");
}

//Disable valve system
void VALVEOFF (char * data) {
	if(data[0] != 0)
		SendLine("Ignoring arguments\r\n");
	PWMTimerStop();
	SendLine("Timer stopped\r\n");
}

//Print the ADC value
void ADCREAD(char * data) {
	SendLine("Current ADC value: ");
	StartADC();
	SendInt(GetADC());
	SendLine("\r\n");
}

//Display date and time of compilation
void INFO (char * data) {
	if(data[0] != 0) {
		SendLine("Ignoring arguments\r\n");
	}
	SendLine("Compilation time and date:\r\n");
	SendLine(__TIME__);
	SendLine("\r\n");
	SendLine(__DATE__);
	SendLine("\r\n");
}

void ERROR (char * data) {
	SendLine("Error: invalid command ");
	SendLine(data);
	SendLine("\r\n");
}

//Enable necessary control structures
void CONTROL_ON (char * data) {
	int temp = StringToInt(data);
	if (temp < 0 ) {
		SendLine("Error: bad argument ");
		SendLine(data);
		SendLine("\r\n");
	}
	//Enable interrupt, set the setpoint, and start conversion
	SETPOINT = (unsigned) temp;
	EnableADCInt();
	StartADC();
}

void CONTROL_OFF (char * data) {
	DisableADCInt();
}
